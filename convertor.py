import requests
import json
import sys

try:
    from openpyxl import Workbook, load_workbook
except ModuleNotFoundError:
    print('''module "openpyxl" not found, try:
pip install openpyxl
or
pip install openpyxl
''')
    sys.exit()


URL = 'https://ec.europa.eu/taxation_customs/sites/default/files/resources/documents/taxation/vat/how_vat_works/rates/vat_rates_en.xlsx'
STATUS_CODE = 200

EXCEL_NAME = "data.xlsx"
JSON_FILE_NAME = 'data.json'

SHEET_NAME = 'List of VAT rates applied'

START_COUNTRY_INDEX = 4
END_COUNTRY_INDEX = 32

COUNTRY_NAME = 'A'
SUPER_REDUCED_RATE = 'C'
REDUCED_RATE = 'D'
STANDARD_RATE = 'E'
PARKING_RATE = 'F'


def get_excel_file():
    """
    function get the excel file from the server and save it on the current path

    return None
    """
    response = requests.get(URL)

    if response.status_code == STATUS_CODE:
        with open(EXCEL_NAME, "wb") as file:
            file.write(response.content)

    else:
        print('Some error accrue when try to download the excel file, try connect to the internet')
        sys.exit()


def convert_excel_data():
    """
    function convert to excel file to json file

    return: None
    """

    data = {}
    wb = load_workbook(EXCEL_NAME)
    ws = wb[SHEET_NAME]

    for col in range(START_COUNTRY_INDEX, END_COUNTRY_INDEX + 1):
        country = ws[COUNTRY_NAME + str(col)].value

        super_reduced_rate = ws[SUPER_REDUCED_RATE + str(col)].value
        reduced_rate = ws[REDUCED_RATE + str(col)].value
        standard_rate = ws[REDUCED_RATE + str(col)].value
        parking_rate = ws[PARKING_RATE + str(col)].value

        temp = {
            "super_reduced_rate": super_reduced_rate,
            "reduced_rate": reduced_rate,
            "standard_rate": standard_rate,
            "parking_rate": parking_rate
        }

        if country == 'null':
            continue

        data[country] = temp

    with open(JSON_FILE_NAME, 'w') as outfile:
        json.dump(data, outfile)


def main():
    get_excel_file()
    convert_excel_data()
    print(f'Convert successfully the excel file to json file: {JSON_FILE_NAME}\n')


if __name__ == '__main__':
    main()
